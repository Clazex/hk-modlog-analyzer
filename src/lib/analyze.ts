export default function analyze(text: string): string[] {
	const prompts: string[] = [];

	if (!text.startsWith('[API] - Starting mod loading')) {
		prompts.push(error('不是合法的 ModLog 文件'));
		console.error('Analyze failed');
		return prompts;
	}

	if (!text.includes('[API] - Finished loading mods:\n')) {
		prompts.push(warn('非最新 API (69 以下)，可能引发错误'));
	}

	if (
		text.includes(
			'System.MissingMethodException: void Modding.ModHooks.add_FinishedLoadingModsHook(System.Action)'
		)
	) {
		prompts.push(error('API 版本过低，请更新 API'));
	}

	if (
		text.includes('[API] - System.TypeLoadException') ||
		text.includes(
			'[API] - System.Reflection.TargetInvocationException: Exception has been thrown by the target of an invocation. ---> System.TypeInitializationException'
		)
	) {
		prompts.push(error('缺少前置或者 API 版本过低'));
	}

	if (
		text.includes(
			'[CustomKnight] - There are no Custom Knight skin folders in the Custom Knight Skins directory.'
		)
	) {
		prompts.push(error('皮肤 Mod 的 Skins 文件夹下缺少 Default 文件夹，请手动创建'));
	}

	if (prompts.length == 0) {
		prompts.push('未发现常见问题');
	}

	console.log('Analyze finished!');
	return prompts;
}

function error(s: string): string {
	return '<div class="prompt error">错误：' + s + '</div>';
}

function warn(s: string): string {
	return '<div class="prompt warn">警告：' + s + '</div>';
}
